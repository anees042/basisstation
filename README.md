OpenQuadroBasisStation
==========
License information:
All open source files in this repository are licensed under the GNU General Public Licence (either version 3 of the License, or [at your option] any later version) unless an alternative licence is provided in source.

OpenQuadroBasisStation implements an interface for your PC to communicate with the OpenQuadro flight controller. Both are based on the STM32F4Discovery. 
